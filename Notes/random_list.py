#!/usr/bin/env python
"""Main file"""

objects = list()
for i in range(10, 50):
    objects.append(i)


print(objects)
print(objects[30:])
print(objects[30:-1])
print(objects[10::2])

#Comprehensions
objects_2 = [i for i in range(10, 50) if i % 2 == 0]

print(objects_2)
print(objects_2[30:])
print(objects_2[30:-1])
print(objects_2[10::2])