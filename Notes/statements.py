#!/usr/bin/env python
"""Main file"""
import sys

name = input("Give me a name")

if not name:
    print("Not a name")
    sys.exit(1)

pi = 5

if name in "Damian":
    pi = 3.14
elif name == "Tomasz":
    pi = 100

print(pi)

number = input("Give me a number")
if number.isalpha():
        if number.startswith("arek"):
            print("Super DUper Exception")
    print("Not a number")
    sys.exit(1)


number = int(number)

if 0<= number <= 5:
    print("Number between 0 and 5 ")
