#!/usr/bin/env python
"""Main file"""

a = [1, 2, 3, None, None]
print(a)
print(a.count(None))
print(bool(a))
print(a.pop())
print(a.pop())
print(a.pop())
print(a)
print(bool(a))

if a:
    print("There's smth")
else:
    print("Empty shell here...")
