#!/usr/bin/env python
"""Main file"""


# a = (1, 2, 3, 4, 5)
# print(a.count(3))
# print(a.index(3))

def get_names():
    return [i for i in ["Arek", "Jarek", "Darecki"]]


for name in get_names():
    print(name)
