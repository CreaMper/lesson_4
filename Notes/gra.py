#!/usr/bin/env python
"""Main file"""
from random import randrange

num = 0

while num != 15:
    num = randrange(5, 25)
    print(num)
