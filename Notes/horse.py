#!/usr/bin/env python
"""Main file"""


class Horse:
        def __init__(self, name:str):
            self.name = name

        def __eq__(self, other):
            return self.name == other.name


horacy = Horse("horacy")
bonifacy = Horse("bonifacy")

print(horacy == bonifacy)