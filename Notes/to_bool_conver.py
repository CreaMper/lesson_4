#!/usr/bin/env python
"""Main file"""


class Human:
    def __init__(self, name: str, is_alive: bool):
        self.name = name
        self.is_alive = is_alive

    def __bool__(self):
        return self.is_alive


a, b, c = Human("a", True), Human("b", True), Human("b", False)

print(bool(a))
print(bool(b))
print(bool(c))
