#!/usr/bin/env python
"""Main file"""


def func(*args, **kwargs):
    print(args)


objects = [1, 2, 3, 4, 5]

func(*objects)

objects_2 = [10, 23, 44, 12, 3, 4, 5, 6]
objects_2.sort()
print(objects_2)
