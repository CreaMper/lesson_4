#!/usr/bin/env python
"""Main file"""

objects = [1, 2, 4, 5, None, "Arecki", True]

objects_2 = [
    [1, 2, 3, 4, 5],
    [1, 2, 3, 4, 5],
    [1, 2, 3, 4, 5]
]

for l in objects_2:
    for el in l:
        print(el, end="")
    print()

print(objects_2[0][2])
