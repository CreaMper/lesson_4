#!/usr/bin/env python
"""Cube capacity"""

__version__ = '1.0.0'
__author__ = 'Wieruchowski Arkadiusz'
__email__ = 'arkadiusz.wieruchowski@outlook.com'
__copyright__ = 'Copyright by Arkadiusz Wieruchowski'


class OfficeStuff:

    def __init__(self, weight: int, amount: int):
        print("Office stuff has been init")
        self.weight: int = weight
        self.amount: int = amount

    def __eq__(self, other):
        """ Checkin' if it's equal"""
        return self.weight == other.weight and (self.amount == other.amount)

    def __lt__(self, other):
        """ Checkin' if it's less"""
        return self.weight < other.weight and (self.amount < other.amount)

    def __gt__(self, other):
        """ Checkin' if it's greater than"""
        return self.weight > other.weight and (self.amount > other.amount)

    def __le__(self, other):
        """ Checkin' if it's less-equal"""
        return self.weight <= other.weight and (self.amount <= other.amount)

    def __ge__(self, other):
        """ Checkin' if it's greater-equal"""
        return self.weight >= other.weight and (self.amount >= other.amount)

    def __ne__(self, other):
        """ Checkin' if it's not equal"""
        return self.weight != other.weight and (self.amount != other.amount)


class Cartons(OfficeStuff):
    def __init__(self, weight: int, amount: int):
        print("Carton has been init")
        super().__init__(weight, amount)
        self.weight: int = weight
        self.amount: int = amount

        def __eq__(self, other):
            """ Checkin' if it's equal"""
            return self.weight == other.weight and (self.amount == other.amount)

        def __lt__(self, other):
            """ Checkin' if it's less"""
            return self.weight < other.weight and (self.amount < other.amount)

        def __gt__(self, other):
            """ Checkin' if it's greater than"""
            return self.weight > other.weight and (self.amount > other.amount)

        def __le__(self, other):
            """ Checkin' if it's less-equal"""
            return self.weight <= other.weight and (self.amount <= other.amount)

        def __ge__(self, other):
            """ Checkin' if it's greater-equal"""
            return self.weight >= other.weight and (self.amount >= other.amount)

        def __ne__(self, other):
            """ Checkin' if it's not equal"""
            return self.weight != other.weight and (self.amount != other.amount)


class Pencils(OfficeStuff):
    def __init__(self, weight: int, amount: int):
        print("Pencil has been init")
        super().__init__(weight, amount)
        self.weight: int = weight
        self.amount: int = amount

        def __eq__(self, other):
            """ Checkin' if it's equal"""
            return self.weight == other.weight and (self.amount == other.amount)

        def __lt__(self, other):
            """ Checkin' if it's less"""
            return self.weight < other.weight and (self.amount < other.amount)

        def __gt__(self, other):
            """ Checkin' if it's greater than"""
            return self.weight > other.weight and (self.amount > other.amount)

        def __le__(self, other):
            """ Checkin' if it's less-equal"""
            return self.weight <= other.weight and (self.amount <= other.amount)

        def __ge__(self, other):
            """ Checkin' if it's greater-equal"""
            return self.weight >= other.weight and (self.amount >= other.amount)

        def __ne__(self, other):
            """ Checkin' if it's not equal"""
            return self.weight != other.weight and (self.amount != other.amount)


class Books(Cartons):
    def __init__(self, weight: int, amount: int):
        print("Books stuff has been init")
        super().__init__(weight, amount)
        self.weight: int = weight
        self.amount: int = amount

        def __eq__(self, other):
            """ Checkin' if it's equal"""
            return self.weight == other.weight and (self.amount == other.amount)

        def __lt__(self, other):
            """ Checkin' if it's less"""
            return self.weight < other.weight and (self.amount < other.amount)

        def __gt__(self, other):
            """ Checkin' if it's greater than"""
            return self.weight > other.weight and (self.amount > other.amount)

        def __le__(self, other):
            """ Checkin' if it's less-equal"""
            return self.weight <= other.weight and (self.amount <= other.amount)

        def __ge__(self, other):
            """ Checkin' if it's greater-equal"""
            return self.weight >= other.weight and (self.amount >= other.amount)

        def __ne__(self, other):
            """ Checkin' if it's not equal"""
            return self.weight != other.weight and (self.amount != other.amount)


class Guides(Books):
    def __init__(self, weight: int, amount: int):
        print("Guides has been init")
        super().__init__(weight, amount)
        self.weight: int = weight
        self.amount: int = amount

        def __eq__(self, other):
            """ Checkin' if it's equal"""
            return self.weight == other.weight and (self.amount == other.amount)

        def __lt__(self, other):
            """ Checkin' if it's less"""
            return self.weight < other.weight and (self.amount < other.amount)

        def __gt__(self, other):
            """ Checkin' if it's greater than"""
            return self.weight > other.weight and (self.amount > other.amount)

        def __le__(self, other):
            """ Checkin' if it's less-equal"""
            return self.weight <= other.weight and (self.amount <= other.amount)

        def __ge__(self, other):
            """ Checkin' if it's greater-equal"""
            return self.weight >= other.weight and (self.amount >= other.amount)

        def __ne__(self, other):
            """ Checkin' if it's not equal"""
            return self.weight != other.weight and (self.amount != other.amount)


class Fantasy(Books):
    def __init__(self, weight: int, amount: int):
        print("Fantasy has been init")
        super().__init__(weight, amount)
        self.weight: int = weight
        self.amount: int = amount

        def __eq__(self, other):
            """ Checkin' if it's equal"""
            return self.weight == other.weight and (self.amount == other.amount)

        def __lt__(self, other):
            """ Checkin' if it's less"""
            return self.weight < other.weight and (self.amount < other.amount)

        def __gt__(self, other):
            """ Checkin' if it's greater than"""
            return self.weight > other.weight and (self.amount > other.amount)

        def __le__(self, other):
            """ Checkin' if it's less-equal"""
            return self.weight <= other.weight and (self.amount <= other.amount)

        def __ge__(self, other):
            """ Checkin' if it's greater-equal"""
            return self.weight >= other.weight and (self.amount >= other.amount)

        def __ne__(self, other):
            """ Checkin' if it's not equal"""
            return self.weight != other.weight and (self.amount != other.amount)


harry_potter = Fantasy(15, 2)
green_pens = Pencils(15, 2)

print(format(harry_potter == green_pens))
