#!/usr/bin/env python
"""Name compare file"""

__version__ = '1.0.0'
__author__ = 'Wieruchowski Arkadiusz'
__email__ = 'arkadiusz.wieruchowski@outlook.com'
__copyright__ = 'Copyright by Arkadiusz Wieruchowski'


name = input("Give me a name: ")

if name in "Arek":
    print("Hi Arek!")
else:
    print("Hi bud!")
