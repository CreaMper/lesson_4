#!/usr/bin/env python
"""Triangle stuff"""

__version__ = '1.0.0'
__author__ = 'Wieruchowski Arkadiusz'
__email__ = 'arkadiusz.wieruchowski@outlook.com'
__copyright__ = 'Copyright by Arkadiusz Wieruchowski'


class Triangle:
    """Class that will be our triangle"""
    def __init__(self, side_length: int):
        """Cnst of triangle"""
        self.side_length: int = side_length

    def __eq__(self, other):
        """ Checkin' if it's equal"""
        return self.side_length == other.side_length

    def __lt__(self, other):
        """ Checkin' if it's less than"""
        return self.side_length < other.side_length

    def __gt__(self, other):
        """ Checkin' if it's greater than"""
        return self.side_length > other.side_length

    def __le__(self, other):
        """ Checkin' if it's less-equal"""
        return self.side_length <= other.side_length

    def __ge__(self, other):
        """ Checkin' if it's greater-equal"""
        return self.side_length >= other.side_length

    def __ne__(self, other):
        """ Checkin' if it's not equal"""
        return self.side_length != other.side_length
